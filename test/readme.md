Tests ported (to Python3.7) and tested. 


Indicates couldn't port or get to work:

- No test description below 

- missing test scripts means couldn't port or get to work.

Working tests
----------------------------

- pycurldownload

Download a file off web, download is buffered, one line progressbar on stdout

Usage

    python3.7 pycurldownload [url]

url is optional. Not provided will download [python-2.5.2.msi](http://www.python.org/ftp/python/2.5.2/python-2.5.2.msi) into scripts folder

After downloading, remember to delete buffer file, foo.txt