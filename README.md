# qmusic

Qt music manager for linux
------------------------------

Motivated to fork this project to get a working app that uses QThreadPool / QRunnables.

Originally a python2.7 app. Ported to python3.7 and added i18n. Locales: English and Traditional Chinese.

Still need help from the community,  which dependencies are needed for QMediaPlayer?

Install hell
--------------

    sudo -H apt-get install --yes python3-pyqt5
    sudo -H apt-get install --yes python3-pyqt5.qtquick
    sudo -H apt-get install --yes python3-pyqt5.qtmultimedia
    
    sudo -H apt-get install --yes qtmultimedia5-dev
    sudo ln -s /usr/lib/x86_64-linux-gnu/libQt5MultimediaQuick_p.so /usr/lib/x86_64-linux-gnu/libQt5MultimediaQuick.so.5
    
**peewee, leveldb, plyvel, lxml**

    sudo -H apt-get install --yes libleveldb-dev
    sudo -H python3.7 -m pip install plyvel

    # from lxml import etree
    sudo -H python3.7 -m pip install --upgrade --ignore-installed --force-reinstall lxml

    # from peewee import *
    sudo -H python3.7 -m pip peewee

    sudo -H python3.7 -m pip mutagen pyquery pycurl chardet faulthandler requests

[qt requirements](http://doc.qt.io/qt-5/linux-requirements.html)

    sudo -H apt-get install --yes gstreamer1.0-qt5
    sudo -H apt-get install --yes libgstreamer1.0-0
    sudo -H apt-get install --yes libgstreamer1.0-dev
    # you might also need to install the following GStreamer plugins: 'good', 'ugly', 'bad', ffmpeg (0.10), and libav (1.x).
    gstreamer1.0-libav
    gstreamer1.0-plugins-good
    gstreamer1.0-plugins-bad
    gstreamer1.0-plugins-ugly
    baresip-ffmpeg
